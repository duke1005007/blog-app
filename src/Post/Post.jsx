import React from "react";

import "./css/bootstrap.min.css";
import "./css/theme.css";
import "./css/style.css";

function Content() {
  return (
    <div>
      <div class="latest_newsarea"></div>

      <div class="category_archive_area">
        <div class="single_archive wow fadeInDown">
          {" "}
          <a href="#">
            <img
              src="https://img.riavrn.ru/upload/preview/4/2/9/4295ba55ae7e0749befeba6a3d4f3182.jpeg"
              alt=""
            />
          </a>{" "}
          <a href="#" class="read_more">
            Читать далее <i class="fa fa-angle-double-right"></i>
          </a>
          <div class="singlearcive_article">
            <h2>
              <a href="#">
                В Воронежской области поймали банду похитителей овец
              </a>
            </h2>
            <a class="author_name" href="#">
              <i class="fa fa-user"></i>Звягинцев Вячеслав Анатольевич
            </a>{" "}
            <a class="post_date" href="#">
              <i class="fa  fa-clock-o"></i>Воскресенье, Сентябрь 24, 2020
            </a>
            <p>
              Полицейские задержали группу мужчин, которые похитили 276 овец,
              сообщила пресс-служба ГУ МВД по Воронежской области во вторник, 17
              марта....
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Content;
