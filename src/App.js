import React, { Component } from "react";
import history from "./history";
import "./App.css";
import Footer from "./Footer/Footer.jsx";
import Blog from "./Blog/Blog.jsx";
import About from "./About/About.jsx";
import Post from "./Post/Post.jsx";
import { BrowserRouter, Router, Switch, Route } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <div>
        <Router history={history}>
          <BrowserRouter>
            <Route path="/" render={() => <Blog />} />
            <Switch>
              <Route path="/About" render={() => <About />} />
              <Route path="/" render={() => <Post />} />
            </Switch>
          </BrowserRouter>
        </Router>
        <Footer />
      </div>
    );
  }
}

export default App;
