import React from "react";

function Footer() {
  return (
    <div>
      <footer id="footer">
        <div class="footer_top">
          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="single_footer_top wow fadeInLeft">
              <h2>Подписаться на рассылку</h2>
              <div class="subscribe_area">
                <p>"Подписка на спам и вирус"</p>
                <form action="#">
                  <div class="subscribe_mail">
                    <input
                      class="form-control"
                      type="email"
                      placeholder="Email адрес"
                    />
                    <i class="fa fa-envelope"></i>
                  </div>
                  <input class="submit_btn" type="submit" value="Подписаться" />
                </form>
              </div>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="single_footer_top wow fadeInRight">
              <h2>Форма обратной связи</h2>
              <form action="#" class="contact_form">
                <label>Имя</label>
                <input class="form-control" type="text" />
                <label>Email*</label>
                <input class="form-control" type="email" />
                <label>Сообщение*</label>
                <textarea class="form-control" cols="30" rows="10"></textarea>
                <input class="send_btn" type="submit" value="Отправить" />
              </form>
            </div>
          </div>
        </div>
        <div class="footer_bottom">
          <div class="footer_bottom_left">
            <p>Авторские права ничем и никем не защищены &copy; 2020</p>
          </div>
          <div class="footer_bottom_right"></div>
        </div>
      </footer>
    </div>
  );
}

export default Footer;
