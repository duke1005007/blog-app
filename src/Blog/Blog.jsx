import React from "react";
import { NavLink } from "react-router-dom";
import history from "../history";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Blog.css";

function Blog() {
  return (
    <div>
      <section id="heroArea">
        <nav class="navbar navbar-expand-lg">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <NavLink
                  to={"/"}
                  onClick={() => history.push("/")}
                  class="nav-link"
                  href=""
                >
                  Главная
                </NavLink>
              </li>
              <li class="nav-item">
                <NavLink
                  to={"/About"}
                  onClick={() => history.push("/About")}
                  class="nav-link"
                  href=""
                >
                  О себе
                </NavLink>
              </li>
            </ul>
          </div>
        </nav>
      </section>
    </div>
  );
}

export default Blog;
