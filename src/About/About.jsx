import React from "react";
import s from "./About.module.css";

function About() {
  return (
    <div>
      <div class="latest_newsarea"></div>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h1>Звягинцев Вячеслав Анатольевич</h1>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <p>
              Звягинцев Вячеслав Анатольевич Мужчина, 21 год, родился 1 ноября
              1998 Проживает: Воронеж Гражданство: Россия, есть разрешение на
              работу: Россия Не готов к переезду, готов к командировкам <br />
              Начинающий специалист <br />• Информационные технологии, Интернет,
              Мультимедиа Занятость: полная занятость График работы: полный день
              Желательное время в пути до работы: не имеет значения .
            </p>
            <p>
              "Ищу компанию, активно использующую для создания пользовательских
              интерфейсов библиотеку React, с целью получения практических
              навыков и дальнейшего развития в web-разработке. Профессиональные
              навыки: HTML5, CSS3, JavaScript, React - самостоятельное изучение.
              Без опыта работы. Web-разработка(front-end react)."
            </p>
          </div>
          <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
            <img
              className={s.photo}
              src="https://sun1-27.userapi.com/WdaEeB2MFjXF299DIv_bZXjg4mv02jQVHW2RIA/SkA9nj9KKv8.jpg"
            />
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <h2>Контакты для связи</h2>
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-12">
            <p>Телефон: +7 (905) 658-81-65</p>
            <p>
              Почта:{" "}
              <a href="mailto: duke1005007@gmail.com">duke1005007@gmail.com</a>
            </p>
            <p>Скайп: SlavaZ</p>
            <p>Телеграм: @Zv_V_A</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default About;
